namespace EVEMon.SkillPlanner
{
    partial class PlanWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlanWindow));
            this.MainStatusStrip = new System.Windows.Forms.StatusStrip();
            this.ObsoleteEntriesStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.SkillsStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.TimeStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.CostStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.upperToolStrip = new System.Windows.Forms.ToolStrip();
            this.tsddbPlans = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsddbSave = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmiPlan = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAfterPlanCharacter = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbDeletePlan = new System.Windows.Forms.ToolStripButton();
            this.tsbPrintPlan = new System.Windows.Forms.ToolStripButton();
            this.tsbCopyForum = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbImplantCalculator = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbAttributesOptimization = new System.Windows.Forms.ToolStripButton();
            this.tsbLoadoutImport = new System.Windows.Forms.ToolStripButton();
            this.ttToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.sfdSave = new System.Windows.Forms.SaveFileDialog();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tpPlanQueue = new System.Windows.Forms.TabPage();
            this.planEditor = new EVEMon.SkillPlanner.PlanEditorControl();
            this.tpSkillBrowser = new System.Windows.Forms.TabPage();
            this.skillBrowser = new EVEMon.SkillPlanner.SkillBrowserControl();
            this.tpShipBrowser = new System.Windows.Forms.TabPage();
            this.shipBrowser = new EVEMon.SkillPlanner.ShipBrowserControl();
            this.tpItemBrowser = new System.Windows.Forms.TabPage();
            this.itemBrowser = new EVEMon.SkillPlanner.ItemBrowserControl();
            this.tpBlueprintBrowser = new System.Windows.Forms.TabPage();
            this.blueprintBrowser = new EVEMon.SkillPlanner.BlueprintBrowserControl();
            this.tpCertificateBrowser = new System.Windows.Forms.TabPage();
            this.certBrowser = new EVEMon.SkillPlanner.CertificateBrowserControl();
            this.ilTabIcons = new System.Windows.Forms.ImageList(this.components);
            this.MainStatusStrip.SuspendLayout();
            this.upperToolStrip.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tpPlanQueue.SuspendLayout();
            this.tpSkillBrowser.SuspendLayout();
            this.tpShipBrowser.SuspendLayout();
            this.tpItemBrowser.SuspendLayout();
            this.tpBlueprintBrowser.SuspendLayout();
            this.tpCertificateBrowser.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainStatusStrip
            // 
            this.MainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ObsoleteEntriesStatusLabel,
            this.SkillsStatusLabel,
            this.TimeStatusLabel,
            this.CostStatusLabel});
            this.MainStatusStrip.Location = new System.Drawing.Point(0, 540);
            this.MainStatusStrip.Name = "MainStatusStrip";
            this.MainStatusStrip.ShowItemToolTips = true;
            this.MainStatusStrip.Size = new System.Drawing.Size(824, 22);
            this.MainStatusStrip.TabIndex = 1;
            // 
            // ObsoleteEntriesStatusLabel
            // 
            this.ObsoleteEntriesStatusLabel.Image = ((System.Drawing.Image)(resources.GetObject("ObsoleteEntriesStatusLabel.Image")));
            this.ObsoleteEntriesStatusLabel.IsLink = true;
            this.ObsoleteEntriesStatusLabel.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.ObsoleteEntriesStatusLabel.Name = "ObsoleteEntriesStatusLabel";
            this.ObsoleteEntriesStatusLabel.Size = new System.Drawing.Size(117, 17);
            this.ObsoleteEntriesStatusLabel.Text = "Obsolete Entries...";
            this.ObsoleteEntriesStatusLabel.Visible = false;
            this.ObsoleteEntriesStatusLabel.Click += new System.EventHandler(this.obsoleteEntriesToolStripStatusLabel_Click);
            // 
            // SkillsStatusLabel
            // 
            this.SkillsStatusLabel.AutoToolTip = true;
            this.SkillsStatusLabel.Image = ((System.Drawing.Image)(resources.GetObject("SkillsStatusLabel.Image")));
            this.SkillsStatusLabel.Name = "SkillsStatusLabel";
            this.SkillsStatusLabel.Size = new System.Drawing.Size(104, 17);
            this.SkillsStatusLabel.Text = "0 Skills Planned";
            this.SkillsStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TimeStatusLabel
            // 
            this.TimeStatusLabel.Image = ((System.Drawing.Image)(resources.GetObject("TimeStatusLabel.Image")));
            this.TimeStatusLabel.Name = "TimeStatusLabel";
            this.TimeStatusLabel.Size = new System.Drawing.Size(116, 17);
            this.TimeStatusLabel.Text = "356d 23h 25m 10s";
            this.TimeStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CostStatusLabel
            // 
            this.CostStatusLabel.Image = ((System.Drawing.Image)(resources.GetObject("CostStatusLabel.Image")));
            this.CostStatusLabel.Name = "CostStatusLabel";
            this.CostStatusLabel.Size = new System.Drawing.Size(98, 17);
            this.CostStatusLabel.Text = "0 ISK Required";
            this.CostStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // upperToolStrip
            // 
            this.upperToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.upperToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsddbPlans,
            this.tsddbSave,
            this.tsbDeletePlan,
            this.tsbPrintPlan,
            this.tsbCopyForum,
            this.toolStripSeparator2,
            this.tsbImplantCalculator,
            this.toolStripSeparator1,
            this.tsbAttributesOptimization,
            this.tsbLoadoutImport});
            this.upperToolStrip.Location = new System.Drawing.Point(0, 0);
            this.upperToolStrip.Name = "upperToolStrip";
            this.upperToolStrip.Size = new System.Drawing.Size(824, 25);
            this.upperToolStrip.TabIndex = 3;
            this.upperToolStrip.Text = "toolStrip1";
            // 
            // tsddbPlans
            // 
            this.tsddbPlans.Image = ((System.Drawing.Image)(resources.GetObject("tsddbPlans.Image")));
            this.tsddbPlans.ImageTransparentColor = System.Drawing.Color.Black;
            this.tsddbPlans.Name = "tsddbPlans";
            this.tsddbPlans.Size = new System.Drawing.Size(93, 22);
            this.tsddbPlans.Text = "Select Plan";
            this.tsddbPlans.DropDownOpening += new System.EventHandler(this.tsddbPlans_DropDownOpening);
            this.tsddbPlans.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsddbPlans_DropDownItemClicked);
            // 
            // tsddbSave
            // 
            this.tsddbSave.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiPlan,
            this.tsmiAfterPlanCharacter});
            this.tsddbSave.Image = ((System.Drawing.Image)(resources.GetObject("tsddbSave.Image")));
            this.tsddbSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsddbSave.Name = "tsddbSave";
            this.tsddbSave.Size = new System.Drawing.Size(69, 22);
            this.tsddbSave.Text = "Export";
            // 
            // tsmiPlan
            // 
            this.tsmiPlan.Image = ((System.Drawing.Image)(resources.GetObject("tsmiPlan.Image")));
            this.tsmiPlan.Name = "tsmiPlan";
            this.tsmiPlan.Size = new System.Drawing.Size(189, 22);
            this.tsmiPlan.Text = "&Plan...";
            this.tsmiPlan.Click += new System.EventHandler(this.tsmiPlan_Click);
            // 
            // tsmiAfterPlanCharacter
            // 
            this.tsmiAfterPlanCharacter.Image = ((System.Drawing.Image)(resources.GetObject("tsmiAfterPlanCharacter.Image")));
            this.tsmiAfterPlanCharacter.Name = "tsmiAfterPlanCharacter";
            this.tsmiAfterPlanCharacter.Size = new System.Drawing.Size(189, 22);
            this.tsmiAfterPlanCharacter.Text = "After Plan &Character...";
            this.tsmiAfterPlanCharacter.Click += new System.EventHandler(this.tsmiAfterPlanCharacter_Click);
            // 
            // tsbDeletePlan
            // 
            this.tsbDeletePlan.Image = ((System.Drawing.Image)(resources.GetObject("tsbDeletePlan.Image")));
            this.tsbDeletePlan.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDeletePlan.Name = "tsbDeletePlan";
            this.tsbDeletePlan.Size = new System.Drawing.Size(69, 22);
            this.tsbDeletePlan.Text = "Delete...";
            this.tsbDeletePlan.Click += new System.EventHandler(this.tsbDeletePlan_Click);
            // 
            // tsbPrintPlan
            // 
            this.tsbPrintPlan.Image = ((System.Drawing.Image)(resources.GetObject("tsbPrintPlan.Image")));
            this.tsbPrintPlan.ImageTransparentColor = System.Drawing.Color.Black;
            this.tsbPrintPlan.Name = "tsbPrintPlan";
            this.tsbPrintPlan.Size = new System.Drawing.Size(61, 22);
            this.tsbPrintPlan.Text = "Print...";
            this.tsbPrintPlan.ToolTipText = "Print this plan";
            this.tsbPrintPlan.Click += new System.EventHandler(this.tsbPrintPlan_Click);
            // 
            // tsbCopyForum
            // 
            this.tsbCopyForum.Image = ((System.Drawing.Image)(resources.GetObject("tsbCopyForum.Image")));
            this.tsbCopyForum.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCopyForum.Name = "tsbCopyForum";
            this.tsbCopyForum.Size = new System.Drawing.Size(124, 22);
            this.tsbCopyForum.Text = "Copy to Clipboard";
            this.tsbCopyForum.Click += new System.EventHandler(this.tsbCopyForum_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbImplantCalculator
            // 
            this.tsbImplantCalculator.Image = ((System.Drawing.Image)(resources.GetObject("tsbImplantCalculator.Image")));
            this.tsbImplantCalculator.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImplantCalculator.Name = "tsbImplantCalculator";
            this.tsbImplantCalculator.Size = new System.Drawing.Size(103, 22);
            this.tsbImplantCalculator.Text = "Implant Calc...";
            this.tsbImplantCalculator.Click += new System.EventHandler(this.tsbImplantCalculator_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbAttributesOptimization
            // 
            this.tsbAttributesOptimization.Image = ((System.Drawing.Image)(resources.GetObject("tsbAttributesOptimization.Image")));
            this.tsbAttributesOptimization.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAttributesOptimization.Name = "tsbAttributesOptimization";
            this.tsbAttributesOptimization.Size = new System.Drawing.Size(128, 22);
            this.tsbAttributesOptimization.Text = "Optimize attributes";
            this.tsbAttributesOptimization.Click += new System.EventHandler(this.tsbAttributesOptimization_Click);
            // 
            // tsbLoadoutImport
            // 
            this.tsbLoadoutImport.Image = ((System.Drawing.Image)(resources.GetObject("tsbLoadoutImport.Image")));
            this.tsbLoadoutImport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLoadoutImport.Name = "tsbLoadoutImport";
            this.tsbLoadoutImport.Size = new System.Drawing.Size(110, 22);
            this.tsbLoadoutImport.Text = "Loadout Import";
            this.tsbLoadoutImport.Click += new System.EventHandler(this.tsbEFTImport_Click);
            // 
            // ttToolTip
            // 
            this.ttToolTip.AutoPopDelay = 5000;
            this.ttToolTip.InitialDelay = 500;
            this.ttToolTip.ReshowDelay = 100;
            // 
            // sfdSave
            // 
            this.sfdSave.Filter = "EVEMon Plan Format (*.emp)|*.emp|XML  Format (*.xml)|*.xml|Text Format (*.txt)|*." +
    "txt";
            this.sfdSave.Title = "Save to File";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tpPlanQueue);
            this.tabControl.Controls.Add(this.tpSkillBrowser);
            this.tabControl.Controls.Add(this.tpCertificateBrowser);
            this.tabControl.Controls.Add(this.tpShipBrowser);
            this.tabControl.Controls.Add(this.tpItemBrowser);
            this.tabControl.Controls.Add(this.tpBlueprintBrowser);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.ImageList = this.ilTabIcons;
            this.tabControl.Location = new System.Drawing.Point(0, 25);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(824, 515);
            this.tabControl.TabIndex = 4;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tpPlanQueue
            // 
            this.tpPlanQueue.Controls.Add(this.planEditor);
            this.tpPlanQueue.ImageIndex = 0;
            this.tpPlanQueue.Location = new System.Drawing.Point(4, 31);
            this.tpPlanQueue.Margin = new System.Windows.Forms.Padding(0);
            this.tpPlanQueue.Name = "tpPlanQueue";
            this.tpPlanQueue.Size = new System.Drawing.Size(816, 480);
            this.tpPlanQueue.TabIndex = 1;
            this.tpPlanQueue.Text = "Plan queue";
            this.tpPlanQueue.UseVisualStyleBackColor = true;
            // 
            // planEditor
            // 
            this.planEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.planEditor.Location = new System.Drawing.Point(0, 0);
            this.planEditor.Name = "planEditor";
            this.planEditor.Plan = null;
            this.planEditor.Size = new System.Drawing.Size(816, 480);
            this.planEditor.TabIndex = 2;
            this.planEditor.TabIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tpSkillBrowser
            // 
            this.tpSkillBrowser.Controls.Add(this.skillBrowser);
            this.tpSkillBrowser.ImageIndex = 1;
            this.tpSkillBrowser.Location = new System.Drawing.Point(4, 31);
            this.tpSkillBrowser.Margin = new System.Windows.Forms.Padding(0);
            this.tpSkillBrowser.Name = "tpSkillBrowser";
            this.tpSkillBrowser.Size = new System.Drawing.Size(816, 480);
            this.tpSkillBrowser.TabIndex = 0;
            this.tpSkillBrowser.Text = "Skill browser";
            this.tpSkillBrowser.UseVisualStyleBackColor = true;
            // 
            // skillBrowser
            // 
            this.skillBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skillBrowser.Location = new System.Drawing.Point(0, 0);
            this.skillBrowser.Name = "skillBrowser";
            this.skillBrowser.Plan = null;
            this.skillBrowser.SelectedSkill = null;
            this.skillBrowser.Size = new System.Drawing.Size(816, 480);
            this.skillBrowser.TabIndex = 0;
            // 
            // tpShipBrowser
            // 
            this.tpShipBrowser.Controls.Add(this.shipBrowser);
            this.tpShipBrowser.ImageIndex = 2;
            this.tpShipBrowser.Location = new System.Drawing.Point(4, 31);
            this.tpShipBrowser.Margin = new System.Windows.Forms.Padding(0);
            this.tpShipBrowser.Name = "tpShipBrowser";
            this.tpShipBrowser.Size = new System.Drawing.Size(816, 480);
            this.tpShipBrowser.TabIndex = 2;
            this.tpShipBrowser.Text = "Ship browser";
            this.tpShipBrowser.UseVisualStyleBackColor = true;
            // 
            // shipBrowser
            // 
            this.shipBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shipBrowser.Location = new System.Drawing.Point(0, 0);
            this.shipBrowser.Name = "shipBrowser";
            this.shipBrowser.Plan = null;
            this.shipBrowser.SelectedObject = null;
            this.shipBrowser.Size = new System.Drawing.Size(816, 480);
            this.shipBrowser.TabIndex = 0;
            // 
            // tpItemBrowser
            // 
            this.tpItemBrowser.Controls.Add(this.itemBrowser);
            this.tpItemBrowser.ImageIndex = 3;
            this.tpItemBrowser.Location = new System.Drawing.Point(4, 31);
            this.tpItemBrowser.Margin = new System.Windows.Forms.Padding(0);
            this.tpItemBrowser.Name = "tpItemBrowser";
            this.tpItemBrowser.Size = new System.Drawing.Size(816, 480);
            this.tpItemBrowser.TabIndex = 3;
            this.tpItemBrowser.Text = "Item browser";
            this.tpItemBrowser.UseVisualStyleBackColor = true;
            // 
            // itemBrowser
            // 
            this.itemBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.itemBrowser.Location = new System.Drawing.Point(0, 0);
            this.itemBrowser.Name = "itemBrowser";
            this.itemBrowser.Plan = null;
            this.itemBrowser.SelectedObject = null;
            this.itemBrowser.Size = new System.Drawing.Size(816, 480);
            this.itemBrowser.TabIndex = 0;
            // 
            // tpBlueprintBrowser
            // 
            this.tpBlueprintBrowser.Controls.Add(this.blueprintBrowser);
            this.tpBlueprintBrowser.ImageIndex = 4;
            this.tpBlueprintBrowser.Location = new System.Drawing.Point(4, 31);
            this.tpBlueprintBrowser.Name = "tpBlueprintBrowser";
            this.tpBlueprintBrowser.Size = new System.Drawing.Size(816, 480);
            this.tpBlueprintBrowser.TabIndex = 5;
            this.tpBlueprintBrowser.Text = "Blueprint browser";
            this.tpBlueprintBrowser.UseVisualStyleBackColor = true;
            // 
            // blueprintBrowser
            // 
            this.blueprintBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.blueprintBrowser.Location = new System.Drawing.Point(0, 0);
            this.blueprintBrowser.Name = "blueprintBrowser";
            this.blueprintBrowser.Plan = null;
            this.blueprintBrowser.SelectedObject = null;
            this.blueprintBrowser.Size = new System.Drawing.Size(816, 480);
            this.blueprintBrowser.TabIndex = 0;
            // 
            // tpCertificateBrowser
            // 
            this.tpCertificateBrowser.Controls.Add(this.certBrowser);
            this.tpCertificateBrowser.ImageIndex = 5;
            this.tpCertificateBrowser.Location = new System.Drawing.Point(4, 31);
            this.tpCertificateBrowser.Name = "tpCertificateBrowser";
            this.tpCertificateBrowser.Padding = new System.Windows.Forms.Padding(3);
            this.tpCertificateBrowser.Size = new System.Drawing.Size(816, 480);
            this.tpCertificateBrowser.TabIndex = 6;
            this.tpCertificateBrowser.Text = "Certificate browser";
            this.tpCertificateBrowser.UseVisualStyleBackColor = true;
            // 
            // certBrowser
            // 
            this.certBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.certBrowser.Location = new System.Drawing.Point(3, 3);
            this.certBrowser.Name = "certBrowser";
            this.certBrowser.Plan = null;
            this.certBrowser.SelectedCertificateClass = null;
            this.certBrowser.Size = new System.Drawing.Size(810, 474);
            this.certBrowser.TabIndex = 0;
            // 
            // ilTabIcons
            // 
            this.ilTabIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilTabIcons.ImageStream")));
            this.ilTabIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.ilTabIcons.Images.SetKeyName(0, "Plan.png");
            this.ilTabIcons.Images.SetKeyName(1, "Skill_24.png");
            this.ilTabIcons.Images.SetKeyName(2, "Ships_24.png");
            this.ilTabIcons.Images.SetKeyName(3, "Items.png");
            this.ilTabIcons.Images.SetKeyName(4, "Blueprint_24.png");
            this.ilTabIcons.Images.SetKeyName(5, "Certificate-24.png");
            // 
            // PlanWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 562);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.upperToolStrip);
            this.Controls.Add(this.MainStatusStrip);
            this.MinimumSize = new System.Drawing.Size(780, 350);
            this.Name = "PlanWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EVEMon Skill Planner";
            this.MainStatusStrip.ResumeLayout(false);
            this.MainStatusStrip.PerformLayout();
            this.upperToolStrip.ResumeLayout(false);
            this.upperToolStrip.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tpPlanQueue.ResumeLayout(false);
            this.tpSkillBrowser.ResumeLayout(false);
            this.tpShipBrowser.ResumeLayout(false);
            this.tpItemBrowser.ResumeLayout(false);
            this.tpBlueprintBrowser.ResumeLayout(false);
            this.tpCertificateBrowser.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip MainStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel SkillsStatusLabel;
        private System.Windows.Forms.ToolStrip upperToolStrip;
        private System.Windows.Forms.ToolStripButton tsbDeletePlan;
        private System.Windows.Forms.ToolTip ttToolTip;
        private System.Windows.Forms.ToolStripButton tsbCopyForum;
        private System.Windows.Forms.SaveFileDialog sfdSave;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbImplantCalculator;
        private System.Windows.Forms.TabPage tpItemBrowser;
        private ItemBrowserControl itemBrowser;
        private System.Windows.Forms.TabPage tpPlanQueue;
        private PlanEditorControl planEditor;
        private System.Windows.Forms.TabPage tpShipBrowser;
        private ShipBrowserControl shipBrowser;
        private System.Windows.Forms.TabPage tpSkillBrowser;
        private SkillBrowserControl skillBrowser;
        private System.Windows.Forms.ToolStripDropDownButton tsddbPlans;
        private System.Windows.Forms.ToolStripButton tsbPrintPlan;
        private System.Windows.Forms.ToolStripDropDownButton tsddbSave;
        private System.Windows.Forms.ToolStripMenuItem tsmiAfterPlanCharacter;
        private System.Windows.Forms.ToolStripMenuItem tsmiPlan;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbAttributesOptimization;
        private System.Windows.Forms.ImageList ilTabIcons;
        private System.Windows.Forms.ToolStripButton tsbLoadoutImport;
        private System.Windows.Forms.ToolStripStatusLabel ObsoleteEntriesStatusLabel;
        private System.Windows.Forms.TabPage tpBlueprintBrowser;
        private BlueprintBrowserControl blueprintBrowser;
        private System.Windows.Forms.ToolStripStatusLabel CostStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel TimeStatusLabel;
        private System.Windows.Forms.TabPage tpCertificateBrowser;
        private CertificateBrowserControl certBrowser;
    }
}
